package kz.wooppay.daniyar.testanimation.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.wooppay.daniyar.testanimation.R;

public class MainActivity extends AppCompatActivity {
    private static final SparseArray<Class<?>> LIST_ITEM_TO_ACTIVITY_MAP = new SparseArray<>();

    static {
        LIST_ITEM_TO_ACTIVITY_MAP.put(R.id.bounce_activity, BounceActivity.class);
        LIST_ITEM_TO_ACTIVITY_MAP.put(R.id.clock_activity, ClockActivity.class);
        LIST_ITEM_TO_ACTIVITY_MAP.put(R.id.grid_activity, GridLayoutActivity.class);
        LIST_ITEM_TO_ACTIVITY_MAP.put(R.id.fragments_activity, FragmentsActivity.class);
        LIST_ITEM_TO_ACTIVITY_MAP.put(R.id.tabbed_activity, TabbedActivity.class);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({
            R.id.bounce_activity,
            R.id.clock_activity,
            R.id.grid_activity,
            R.id.fragments_activity,
            R.id.tabbed_activity,
    })
    void onClick(View v){
        Intent intent = new Intent(getApplicationContext(), LIST_ITEM_TO_ACTIVITY_MAP.get(v.getId()));
        startActivity(intent);
    }

}
