package kz.wooppay.daniyar.testanimation.adapter;

import android.support.v7.widget.helper.ItemTouchHelper;

interface ItemTouchHelperAdapter {

    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);

}
