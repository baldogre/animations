package kz.wooppay.daniyar.testanimation.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import kz.wooppay.daniyar.testanimation.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentsActivityFragment extends Fragment {

    public FragmentsActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        
        return inflater.inflate(R.layout.fragment_fragments, container, false);
    }

}
