package kz.wooppay.daniyar.testanimation.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.wooppay.daniyar.testanimation.R;
import kz.wooppay.daniyar.testanimation.adapter.RecyclerViewAdapter;
import kz.wooppay.daniyar.testanimation.adapter.SimpleItemTouchHelperCallback;
import kz.wooppay.daniyar.testanimation.Utility;

public class GridLayoutActivity extends AppCompatActivity {

    @BindView(R.id.view)
    RecyclerView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_layout);

        ButterKnife.bind(this);

        int size = Utility.calculateNoOfColumns(this);

        GridLayoutManager manager = new GridLayoutManager(this, size);
        view.setLayoutManager(manager);

        ArrayList<String> strings = new ArrayList<>();

        for (int i = 0; i < size*size; i++) {
            strings.add("item #" + i);
        }

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(strings);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(view);
        view.setAdapter(adapter);


    }
}