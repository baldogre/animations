package kz.wooppay.daniyar.testanimation.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import kz.wooppay.daniyar.testanimation.R;
import kz.wooppay.daniyar.testanimation.ReverseInterpolator;

public class BounceActivity extends AppCompatActivity {
    ImageView imageView;
    ScaleAnimation animation;
    ReverseInterpolator interpolator;

//    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bounce);
        imageView = findViewById(R.id.text_view);

        interpolator = new ReverseInterpolator();
        animation = new ScaleAnimation(0.2f, 1f, 0.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(2000);
        animation.setInterpolator(interpolator);
        imageView.setAnimation(animation);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        animation.start();

    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        interpolator.setmB(interpolator.getmB() + 0.2f);
        return super.onTouchEvent(event);
    }
}
