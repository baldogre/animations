package kz.wooppay.daniyar.testanimation;

import android.util.Log;
import android.view.animation.Interpolator;

public class ReverseInterpolator implements Interpolator {
    private float mB = 10;
    private float mA = 1;
    public static final int c = 30;

    @Override
    public float getInterpolation(float input) {
        if (mB > 10) {
            mB -= 0.1f;
        }
//        Log.d("Interpolator", mB + "");
        return (float) (-1 * Math.pow(Math.E, -1 * input / mA) *
                Math.cos(mB * input) + 1);
    }

    public void setmB(float mB) {
        if (mB > c) {
            mB -= mB % c;
        }
        this.mB = mB;
    }

    public void setmA(float mA) {
        this.mA = mA;
    }

    public float getmB() {
        return mB;
    }

    public float getmA() {
        return mA;
    }
}
