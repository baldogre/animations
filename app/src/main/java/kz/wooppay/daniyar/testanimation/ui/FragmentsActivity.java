package kz.wooppay.daniyar.testanimation.ui;

import android.animation.ValueAnimator;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import kz.wooppay.daniyar.testanimation.R;

public class FragmentsActivity extends AppCompatActivity {

    BlankFragment fragment;
    FragmentsActivityFragment fragmentsActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragment = new BlankFragment();
        fragmentsActivity = new FragmentsActivityFragment();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.open_fragment, R.animator.close_fragment);
        transaction.add(R.id.fragment_container, fragment);
        transaction.commit();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.animator.open_fragment, R.animator.close_fragment);
                if (fragment.isVisible()) {
                    transaction.remove(fragment);
                    transaction.add(R.id.fragment_container, fragmentsActivity);
                } else {
                    transaction.remove(fragmentsActivity);
                    transaction.add(R.id.fragment_container, fragment);
                }
                transaction.commit();
            }
        });
    }

}
