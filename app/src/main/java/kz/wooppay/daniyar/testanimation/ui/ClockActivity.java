package kz.wooppay.daniyar.testanimation.ui;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.graphics.drawable.Animatable2Compat;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.wooppay.daniyar.testanimation.R;

public class ClockActivity extends AppCompatActivity {



    @BindView(R.id.search_to_back)
    ImageView searchToBack;
    enum State {
        SEARCH_TO_BACK,
        BACK_TO_SEARCH,
    }

    State state = State.SEARCH_TO_BACK;


    @BindView(R.id.clock)
    ImageView clock;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clock);

        ButterKnife.bind(this);

//        Animatable2.AnimationCallback callback = new Animatable2.AnimationCallback() {
//            @Override
//            public void onAnimationEnd(Drawable drawable) {
//
//        };


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(final View v) {
                Drawable drawable = ((ImageView) v).getDrawable();
                if (drawable instanceof Animatable2) {
                    v.setClickable(false);
                    ((Animatable2) drawable).registerAnimationCallback(new Animatable2.AnimationCallback() {
                        @Override
                        public void onAnimationEnd(Drawable drawable) {
                            if (v.getId() == R.id.search_to_back) {
                                switch (state) {
                                    case BACK_TO_SEARCH:
                                        ((ImageView) v).setImageResource(R.drawable.search_to_back);
                                        state = State.SEARCH_TO_BACK;
                                        break;
                                    case SEARCH_TO_BACK:
                                        ((ImageView) v).setImageResource(R.drawable.back_to_search);
                                        state = State.BACK_TO_SEARCH;
                                        break;
                                }
                            }
                            v.setClickable(true);
                        }

                        }
                    );
                    ((Animatable2) drawable).start();

                } else {
                    Log.d("ClockActivity", "Can't show animation :(");
                }
            }
        };



        clock.setOnClickListener(onClickListener);
        searchToBack.setOnClickListener(onClickListener);


    }
}
